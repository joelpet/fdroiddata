Categories:
  - Time
License: Apache-2.0
SourceCode: https://github.com/plusonelabs/calendar-widget
IssueTracker: https://github.com/plusonelabs/calendar-widget/issues
Changelog: https://github.com/plusonelabs/calendar-widget/releases

AutoName: Todo Agenda
Description: |-
    The Todo Agenda is a home screen widget. It displays a list of upcoming calendar
    events so that you can easily have a glimpse at your upcoming appointments.

    Features:

    * Displays all events from your calendars. Optionally shows past events.
    * Automatically updates when you add/delete/modify an event. Or you may update the list instantly.
    * Select only the calendars you want to see in the widget.
    * Create several widgets, if you need. Each widget will have its own settings, including filters and selected calendars.
    * Customize colors of the widget background and texts.
    * Scroll through the list of upcoming events.
    * Customize the text size of the widget.
    * Fully resizable widget with two alternative layouts.
    * Indicators for alerts and recurring events.
    * Lock time zone when travelling to different time zones.
    * Turn off Day headers and see dates in the "Days from today" column.
    * Supports tablets.

RepoType: git
Repo: https://github.com/plusonelabs/calendar-widget.git

Builds:
  - versionName: 1.6.2
    versionCode: 14
    commit: c80eddb1
    subdir: app/com.plusonelabs.calendar

  - versionName: 1.6.4
    versionCode: 16
    commit: v1.6.4
    subdir: app/com.plusonelabs.calendar

  - versionName: 1.7.2
    versionCode: 19
    commit: v1.7.2
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: '1.8'
    versionCode: 20
    commit: v1.8
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 1.8.1
    versionCode: 21
    commit: v1.8.1
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 1.8.2
    versionCode: 22
    commit: v1.8.2
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 1.8.3
    versionCode: 23
    commit: v1.8.3
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 1.8.4
    versionCode: 24
    commit: v1.8.4
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 1.8.5
    versionCode: 25
    commit: v1.8.5
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 1.8.6
    versionCode: 26
    commit: v1.8.6
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 1.9.0
    versionCode: 27
    commit: v1.9
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 1.9.1
    versionCode: 28
    commit: v1.9.1
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 1.9.2-6b6d980
    versionCode: 334
    commit: v1.9.2
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 1.9.3-35a0ac1
    versionCode: 347
    commit: v1.9.3
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 1.10.0-e7438daa
    versionCode: 408
    commit: v1.10.0
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 1.10.1-4853a036
    versionCode: 414
    commit: v1.10.1
    subdir: app/calendar-widget
    gradle:
      - yes

  - versionName: 2.0.1-425
    versionCode: 425
    commit: v2.0.1
    subdir: app
    gradle:
      - yes
    prebuild: sed -i -e '/defaultConfig/a\        applicationId "com.plusonelabs.calendar"'
        build.gradle

MaintainerNotes: |-
    Upstream generates CV and CVC from git, so we don't catch that anymore,
    see https://github.com/plusonelabs/calendar-widget/issues/210.

    Application ID changed in 2.0.0, but for F-Droid we keep the old one, see
    https://github.com/plusonelabs/calendar-widget/issues/291.

AutoUpdateMode: None
UpdateCheckMode: None
CurrentVersion: 2.0.1-425
CurrentVersionCode: 425
